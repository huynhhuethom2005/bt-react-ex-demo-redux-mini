import React, { Component } from 'react'
import { connect } from 'react-redux'

class DemoReduxMini extends Component {
    render() {
        console.log('this.props :>> ', this.props);
        return (
            <div className='mt-5'>
                <button className="btn btn-danger" onClick={this.props.handleTang}>+</button>
                <strong className="col-1">{this.props.soLuong}</strong>
                <button className="btn btn-success" onClick={this.props.handleGiam}>-</button>
            </div>
        )
    }
}

// Lấy data từ Store của redux
let mapStateToProps = (state) => {
    return {
        soLuong: state.number.number
        // key: tên props
        // value: giá trị của state REDUX
    }
}

// Set dispatch function
let mapDispatchToProps = (dispatch) => {
    return {
        handleTang: () => {
            let action = {
                type: "TANG_SO_LUONG"
            }
            dispatch(action)
        },

        handleGiam: () => {
            let action = {
                type: "GIAM_SO_LUONG",
                payload: 5,
            }
            dispatch(action)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini)
