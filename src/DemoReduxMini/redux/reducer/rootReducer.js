import { combineReducers } from "redux"
import { numberRender } from "./numberReducer"

export const rootReducer_DemoReduxMini = combineReducers({
    number: numberRender
})

// key: đại diện cho giá trị của sate trong reducer
// value: name reducer
