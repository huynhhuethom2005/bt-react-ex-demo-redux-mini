let initialValue = { number: 1 }

export const numberRender = (state = initialValue, action) => {
    switch (action.type) {
        case 'TANG_SO_LUONG': {
            state.number += 10
            return { ...state }
        }
        case 'GIAM_SO_LUONG': {
            switch (state.number) {
                case 1: {
                    return state
                } default: {
                    state.number -= action.payload 
                    return { ...state }
                }
            }
        }
        default: {
            return state
        }
    }
}
